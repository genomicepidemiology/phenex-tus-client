import { fingerprint } from '../src/fingerprint.js';

test('use jsdom in this test file', () => {
  const element = document.createElement('div');
  expect(element).not.toBeNull();
});

describe('fingerprint suite', function() {
  class MockFile {
    constructor() {}
    create(size = 1024, name = 'mock.txt', mimeType = 'plain/txt') {
      function range(count) {
        var output = '';
        for (var i = 0; i < count; i++) {
          output += 'a';
        }
        return output;
      }
      let blob = new Blob([range(size)], { type: mimeType });
      blob.lastModifiedDate = new Date();
      blob.name = name;
      return blob;
    }
  }
  test('check mock file class', function() {
    let file = new MockFile();
    expect(file).not.toBeNull();
  });

  test('should have default values', function() {
    let mock = new MockFile();
    let file = mock.create();
    expect(file.name).toBe('mock.txt');
    expect(file.size).toBe(1024);
  });

  test('hash should be a', async done => {
    let mock = new MockFile();
    let file = mock.create(2097152);

    function callback(hash) {
      try {
        expect(hash).toBe('de89461b64701958984c95d1bfb0065a');
        done();
      } catch (e) {
        done(e);
      }
    }

    fingerprint(file, callback);
  });

  test('hash should be ', async done => {
    let mock = new MockFile();
    let file = mock.create(2097152);

    function callback(hash) {
      try {
        expect(hash).toBe('7202826a7791073fe2787f0c94603278');
        done();
      } catch (e) {
        done(e);
      }
    }

    fingerprint(file, callback, 1);
  });
});
