import {
  has_metadata,
  get_xlsx_files,
  validate_upload_files
} from '../src/file_validation/validation_service';

describe('xml existence suite', function() {
  class MockFile {
    constructor() {}
    create(size = 1024, name = 'mock.txt', mimeType = 'plain/txt') {
      function range(count) {
        var output = '';
        for (var i = 0; i < count; i++) {
          output += 'a';
        }
        return output;
      }
      let blob = new Blob([range(size)], { type: mimeType });
      blob.lastModifiedDate = new Date();
      blob.name = name;
      return blob;
    }
  }

  test('has_metadata should return true if there is xlsx file', () => {
    const mock = new MockFile();
    const metadata = mock.create(
      1024,
      'mock.xlsx',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    const file = mock.create();

    const fileList = [metadata, file];

    expect(has_metadata(fileList)).toBe(true);
  });

  test('has_metadata should return false if xlsx is missing', () => {
    const mock = new MockFile();
    const file = mock.create();
    const file2 = mock.create(2048);
    const fileList = [file, file2];

    expect(has_metadata(fileList)).toBe(false);
  });

  test('get_xlsx_files should return xlsx file from file list', () => {
    const mock = new MockFile();
    const metadata = mock.create(
      1024,
      'mock.xlsx',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    const file = mock.create(2048);
    const fileList = [file, metadata];
    

    expect(get_xlsx_files(fileList)).toStrictEqual([metadata]);
  });

  //Integration test
  test('validate_upload_files should return error calback if receives multiple xlsx from get_xlsx_files', () => {
    const mock = new MockFile();
    const metadata1 = mock.create(
      1024,
      'mock.xlsx',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    const metadata2 = mock.create(
      1024,
      'mock2.xlsx',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    const file = mock.create(2048);
    const fileList = [file, metadata1, metadata2];
    
    const success = () => {};
    const fail = (errors) => {
      return errors
    };

    expect(validate_upload_files(fileList, success, fail)).toStrictEqual(['multiple xlsx files in upload. 1 xlsx metadata file required']);
  });
});
