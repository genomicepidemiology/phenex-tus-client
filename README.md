# Tusfull client javascript 

## Usage

Following code is needed to be add to your HTML:

#### Inside page header

```html
<link rel="stylesheet" href="pathe_to_uploder_css/uploder.css">
```

#### Between <body> </body> tags

To place where the upload element should be:

```html
<div id="uploader-drop-area">
    <form>
      <p>Upload multiple files</p>
    </form>
</div>
```

if you use CSRF protection add into the form a input with value of csrf token. In case of Flask:

```html
<input type="hidden" name="csrf_token" value="{{ csrf_token() }}" />
```

#### Before end of </body> 

```html
<script src="PATH_TO_BUNDLE/dtu_tusfull_client.min.js')}}"></script>
<script>
  let endpoint_url = "http://" + "TUSFULL_SERVER_URL" + "/files/";
  let uploder = dtu_tus_client.init(endpoint_url, '{{ csrf_token() }}');
  //In case of CSRF and flask use following instead:
  // let uploder = dtu_tus_client.init(endpoint_url, '{{ csrf_token() }}')
</script>
```

Bundle file **dtu_tusfull_client.min.js** is generated into dist folder of repository

## Development

### Set up

1. clone repository

2. in repository folder run comand `npm install`

3. compile module into bundle using 
   `npm run webpack`

   in case you want webpack compilation to run contuously on every change add -watch flag

   ```bash
   npm run webpack -watch
   ```

4.  Compiled **dtu_tusfull_client.min.js** is in **./dist** directory

