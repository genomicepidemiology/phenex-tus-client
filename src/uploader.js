import handleFiles from './handle_files.js';
import setAnoumousId from './samples/anonymousId.js';
import { initialiseUpload, startFileUpload, sendSampleMetadata } from './networking';

export default class SampleUploader {
  options = {
    headers: {},
    chunkSize: 15 * 1024 * 1024, // n MiB
    removeFingerprintOnSuccess: true
  };

  constructor(endpoinsts, callbacks, tus_options = null, csrf_token = null) {
    this.sample_endpoint = endpoinsts.samples;
    this.csrf_token = csrf_token;
    this.callbacks = {
      ...this.callbacks,
      ...callbacks
    };

    this.options = {
      ...this.options,
      ...tus_options,
      endpoint: endpoinsts.files
    };

    if (csrf_token) {
      this.options.headers['X-CSRFToken'] = csrf_token;
    }

    setAnoumousId();
  }

  async handleFiles(files, sampleMetadata) {
    let samples = await handleFiles(files, sampleMetadata, this.callbacks);
    if (samples) {
      await this.upload(samples);
    }
    return samples;
  }

  async upload(object) {
    try {
      if (Array.isArray(object)) {
        for (let index = 0; index < object.length; index++) {
          await this.__handleSampleUpload(object[index]);
        }
      } else {
        await this.__handleSampleUpload(object);
      }
    } catch (error) {
      this.callbacks.onError(error.message);
    }
  }

  async __handleSampleUpload(sample) {
    let validSampleFormat =
      Object.prototype.hasOwnProperty.call(sample, 'files') &&
      Object.prototype.hasOwnProperty.call(sample, 'fingerprint') &&
      Array.isArray(sample.files) &&
      Array.isArray(sample.fingerprint);

    if (validSampleFormat) {
      let response = await sendSampleMetadata(sample, this.sample_endpoint, this.csrf_token);
      if (response.success) {
        for (let index = 0; index < sample.files.length; index++) {
          await this.__handleFileUplode(sample.files[index], response.data.sample_id);
        }
      } else {
        this.callbacks.onError(response.message);
      }
    } else {
      this.callbacks.onError(
        'Sample object has wrong format. Key-value pairs files:Array and fingerprint:Array required'
      );
    }
  }

  async __handleFileUplode(file, sample_id) {
    let file_url = await initialiseUpload(file, this.options);
    await startFileUpload(file_url, file, sample_id, this.callbacks, this.options);
  }
}
