/* eslint-disable no-console */
import tus from 'tus-js-client';
import Storage from './samples/storage.js';

export async function sendSampleMetadata(sample, endpoint_url, csrf_token = null) {
  let headers = { 'Content-Type': 'application/json' };
  let metadata = { ...sample };
  delete metadata.files;
  delete metadata.uuid;
  metadata = JSON.stringify(metadata);

  if (csrf_token) {
    headers['X-CSRFToken'] = csrf_token;
  }

  let response = await fetch(endpoint_url, {
    method: 'POST',
    headers: headers,
    body: metadata
  });

  if (response.ok) {
    return await response.json();
  }
  throw new Error(`status: ${response.status} mesage: ${response.statusText}`);
}

export async function initialiseUpload(file, options) {
  // base64 encode fingeprint and filename
  const INITIAL_HEADERS = {
    'Tus-Resumable': '1.0.0',
    'Upload-Length': file.size,
    'Upload-Metadata': `fingerprint ${btoa(file.md5Fingerprint)},filename ${btoa(file.name)},anonymous_id ${btoa(Storage.getCookie("anonymous_id"))}`,
    ...options.headers
  };

  let response = await fetch(options.endpoint, { method: 'POST', headers: INITIAL_HEADERS });
  if (response.ok) {
    let file_url = response.headers.get('Location');
    return file_url;
  }

  throw new Error(`status: ${response.status} mesage: ${response.statusText}`);
}

export function startFileUpload(file_url, file, sample_id, callbacks, options) {
  return new Promise((resolve, reject) => {
    let onSuccess = function() {
      if ('onFileUploadSuccess' in callbacks) {
        callbacks.onFileUploadSuccess(file);
      }
      resolve();
    };

    let onError = function(error) {
      if ('onError' in callbacks) {
        callbacks.onError(error);
      }
      reject(error);
    };

    let onProgress = function(uploadedBytes, totalBytes) {
      let progressPercentage = 0;
      if (totalBytes) {
        progressPercentage = ((uploadedBytes / totalBytes) * 100).toFixed(2);
      }
      if ('onFileUploadProgress' in callbacks) {
        callbacks.onFileUploadProgress(progressPercentage, file);
      }
    };


    let tus_options = {
      ...options,
      headers: {...options.headers, Sample: btoa(sample_id)},
      onSuccess: onSuccess,
      onError: onError,
      onProgress: onProgress
    };

    let originalTus = new tus.Upload(file, tus_options);

    // assing file endpoint to uploader so it starts uploading in resume mode.
    originalTus.url = file_url;
    originalTus.start();
  });
}
