export default {
  readArrayBufferPromise: function(file, START_BYTE=0, STOP_BYTE=0) {
    let reader = new FileReader()
    reader.file = file

    return new Promise(function(resolve, reject) {
      reader.onerror = function() {
        reader.abort()
        reject("Problem parsing input file.")
      }

      reader.onload = function(e) {
        return resolve({ event: e, data: reader.result })
      }

      if (START_BYTE == STOP_BYTE) {
        reader.readAsArrayBuffer(file)
      } else {
        reader.readAsArrayBuffer(file.slice(START_BYTE, STOP_BYTE))
      }
    })
  },
  readAsTextPromise: function(file, START_BYTE=0, STOP_BYTE=0) {
    let reader = new FileReader()
    reader.file = file

    return new Promise(function(resolve, reject) {
      reader.onerror = function() {
        reader.abort()
        reject("Problem parsing input file.")
      }

      reader.onload = function(e) {
        return resolve({ event: e, data: reader.result })
      }

      if (START_BYTE == STOP_BYTE) {
        reader.readAsText(file)
      } else {
        reader.readAsText(file.slice(START_BYTE, STOP_BYTE))
      }
    })
  },
  readAsBinaryStringPromise: function(file, START_BYTE=0, STOP_BYTE=0) {
    let reader = new FileReader()
    reader.file = file

    return new Promise(function(resolve, reject) {
      reader.onerror = function() {
        reader.abort()
        reject("Problem parsing input file.")
      }

      reader.onload = function(e) {
        return resolve({ event: e, data: reader.result })
      }

      if (START_BYTE == STOP_BYTE) {
        reader.readAsBinaryString(file)
      } else {
        reader.readAsBinaryString(file.slice(START_BYTE, STOP_BYTE))
      }
    })
  }
}
