import Sample from './sample.js';
import XLSXMetaExtractor from './xlsxMetadataExtractor.js';

export default class SampleExtractor {
  files = [];
  samples = [];
  xlsxExtractor = undefined;

  constructor(files) {
    this.files = files;
  }

  async extract() {
    if (this.files.length == 1 || this.files.length == 2) {
      let files = this.getSequencingFiles();
      let sample = new Sample(files);
      this.samples.push(sample);
    } else {
      await this.xlsxExtractMetadata();
      const CELL_SELECTOR = /^A\d+$/;
      const SAMPLE_DELIMITER = /,\s|,|\s/;
      for (let cell in this.xlsxExtractor.metadata) {
        if (cell != 'A1' && cell.match(CELL_SELECTOR)) {
          let filesName = this.xlsxExtractor.metadata[cell].v.split(SAMPLE_DELIMITER);
          let files = this.getSequencingFiles(filesName);
          let sample = new Sample(files, cell);
          this.samples.push(sample);
        }
      }
    }
  }

  getSequencingFiles(filesName = undefined) {
    if (filesName === undefined) {
      return this.files.filter(function(file) {
        return !file.name.endsWith('xlsx');
      });
    } else {
      let files = [];
      for (let i = 0; i < filesName.length; i++) {
        let filename = filesName[i];
        let file = this.files.filter(function(file) {
          return file.name.endsWith(filename);
        }, filename);
        files.push(file[0]);
      }
      return files;
    }
  }

  async xlsxExtractMetadata() {
    let xlsxFile = this.getXLSXFile();
    this.xlsxExtractor = new XLSXMetaExtractor(xlsxFile);
    await this.xlsxExtractor.extract();
  }

  getXLSXFile() {
    return this.files.filter(function(file) {
      return file.name.endsWith('xlsx');
    })[0];
  }
}
