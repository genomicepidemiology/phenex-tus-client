export default {
  arrayBufferToHex: function (arrBuf) {
    var hex = ''
    var arr = new Uint8Array(arrBuf)
    for (var i = 0; i < arr.length; i++) {
      hex += arr[i].toString(16)
    }
    return hex
  },
  arrayToN(array, n) {
    // transform array from [12345...] to [[1...N],[1...N]...]
    var tmp = []
    var normArray = []
    var stopIndex = Math.floor(array.length / n) * n
    for (var i = 0; i <= stopIndex; i++) {
      if (i != 0 && i % n == 0) {
        normArray.push(tmp)
        tmp = []
      }
      tmp.push(array[i])
    }
    normArray.pop()
    return normArray
  }
}
