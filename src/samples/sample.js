import { calculateMD5Hash } from './fingerprint.js';
import Storage from './storage.js';

export default class Sample {
  files = [];
  fingerprint = [];
  cell = '';

  constructor(files, cell) {
    this.cell = cell;
    this.files = files;
    this.anonymous_id = Storage.getCookie('anonymous_id');
  }

  addMetadata(metadata) {
    for (let key in metadata) {
      if (Object.prototype.hasOwnProperty.call(metadata, key)) {
        this[key] = metadata[key];
      }
    }
  }

  indexFiles() {
    for (let i = 0; i < this.files.length; i++) {
      this.files[i]['index'] = i;
      this.files[i]['cell'] = this.cell;
    }
  }

  initializeFileUploadProgress() {
    for (let i = 0; i < this.files.length; i++) {
      this.files[i]['uploadProgress'] = 0; // %
    }
  }

  async generateFingerprint(bytesToCalculate = 0, algorithm = 'md5') {
    const MiB = 1048576;
    for (let i = 0; i < this.files.length; i++) {
      let file = this.files[i];
      bytesToCalculate = bytesToCalculate != 0 ? bytesToCalculate : file.size;
      if (algorithm == 'md5') {
        let md5Hash = await calculateMD5Hash(file, bytesToCalculate, 5 * MiB);
        this.files[i]['md5Fingerprint'] = md5Hash;
        this.fingerprint[i] = md5Hash;
      }
    }
  }
}
