import SparkMD5 from 'spark-md5';

export function calculateMD5Hash(file, bytesToCalculate = 0, chunkSizeInBytes = 1048576) {
  let spark = new SparkMD5.ArrayBuffer();
  let blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice;

  let currentChunk = 0;
  const TOTAL_CHUNKS = (function() {
    if (bytesToCalculate >= file.size) {
      return Math.ceil(file.size / chunkSizeInBytes);
    }
    return Math.ceil(bytesToCalculate / chunkSizeInBytes);
  })();

  return new Promise(function(resolve, reject) {
    var reader = new FileReader();

    loadNext();

    function loadNext() {
      var startByte = 0;
      var stopByte = bytesToCalculate;
      if (bytesToCalculate > chunkSizeInBytes) {
        startByte = currentChunk * chunkSizeInBytes;
        stopByte =
          startByte + chunkSizeInBytes >= file.size ? file.size : startByte + chunkSizeInBytes;
      }
      reader.readAsArrayBuffer(blobSlice.call(file, startByte, stopByte));
    }

    reader.onload = function(e) {
      // Append file content as ArrayBuffer
      spark.append(e.target.result);

      currentChunk++;

      if (currentChunk < TOTAL_CHUNKS) {
        loadNext();
      } else {
        let hash = spark.end();
        resolve(hash);
      }
    };

    reader.onerror = function() {
      console.warn('Error in calculateMD5Hash occured');
      reject('Error in calculateMD5Hash occured');
    };
  });
}
