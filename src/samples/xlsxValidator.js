var Levenshtein = require('fast-levenshtein');
import XLSXMetaExtractor from './xlsxMetadataExtractor.js';

export default class XLSXValidator {
  files = [];
  valid = true;
  errors = [];
  extractor = undefined;
  grammar = {
    columns: ['file_name(s)']
  };

  constructor(files) {
    this.files = files;
  }

  async validate() {
    let valid, error;
    let fileCount = this.getFileCount();

    switch (fileCount) {
      case 1:
        if (this.files[0].name.endsWith('xlsx')) {
          [valid, error] = [false, 'XLSX file is missing sample files.'];
        } else {
          [valid, error] = this.validateForOneFile();
        }
        break;
      case 2:
        [valid, error] = this.validateForTwoFiles();
        break;
      default:
        [valid, error] = await this.validateForManyFiles();
    }

    this.updateValidator(valid, error);
  }

  validateForOneFile() {
    let [valid, error] = this.validateFilesName();
    if (!valid) {
      return [false, error];
    }
    return [true, ''];
  }

  validateForTwoFiles() {
    let valid = true;
    let errors = [];

    let [validateFilesName, validateFilesNameError] = this.validateFilesName();
    if (!validateFilesName) {
      valid = false;
      errors.push(...validateFilesNameError);
    }

    let names = this.getFilesName();
    let [validateLevenstein, validateLevensteinError] = this.validateLevenstein(names);
    if (!validateLevenstein) {
      valid = false;
      errors.push(...validateLevensteinError);
    }

    return [valid, errors];
  }

  async validateForManyFiles() {
    let [valid, error] = this.validateFilesName();
    if (!valid) {
      return [false, error];
    }

    if (!this.hasXLSXFile()) {
      return [false, `Sample is missinx XLSX file.`];
    }

    if (!this.hasOnlyOneXLSXFile()) {
      return [false, `Detected multiple XLSX files. Only one XLSX file is required.`];
    }

    // Extract metadata from xlsx sheet
    await this.extractMetadata();

    if (!this.sheetHasMetadata()) {
      return [false, `XLSX file is corrupted or is missing 'metadata' sheet.`];
    }

    if (!this.sheetHasColumn('file_name(s)')) {
      return [false, `Metadata sheet is missing 'file_name(s)' column.`];
    }

    if (!this.sheetHasData()) {
      return [false, `Metadata sheet is missing content in 'file_name(s)' column.`];
    }

    let [constantLenght, cell] = this.sampleHaveConstantLength();
    if (!constantLenght) {
      return [false, `Samples have various length. Please fix sample at ${cell} cell.`];
    }

    let [sampleHaveFiles, sampleHaveFilesErrors] = this.sampleHaveFiles();
    if (!sampleHaveFiles) {
      return [false, sampleHaveFilesErrors];
    }

    let [
      sampleHasValidLevenstein,
      sampleHasValidLevensteinErrors
    ] = this.sampleHasValidLevenstein();
    if (!sampleHasValidLevenstein) {
      return [false, sampleHasValidLevensteinErrors];
    }

    return [true, ''];
  }

  async extractMetadata() {
    let xlsxFile = this.getXLSXFile();
    this.extractor = new XLSXMetaExtractor(xlsxFile);
    await this.extractor.extract();
  }

  sampleHasValidLevenstein() {
    let valid = true;
    let errors = [];
    const CELL_SELECTOR = /^A\d+$/;
    const SAMPLE_DELIMITER = /,\s|,|\s/;
    for (let cell in this.extractor.metadata) {
      if (cell != 'A1' && cell.match(CELL_SELECTOR)) {
        let filesName = this.extractor.metadata[cell].v.split(SAMPLE_DELIMITER);
        let [v, err] = this.validateLevenstein(filesName);
        let normalizedError = err.map(function(errMsg) {
          return errMsg + ` Please check ${cell} cell as well as neighbouring cells.`;
        }, cell);
        valid = v;
        errors.push(...normalizedError);
      }
    }
    return [valid, errors];
  }

  sampleHaveConstantLength() {
    const SAMPLE_DELIMITER = /,\s|,|\s/;
    const LENGTH_OF_1ST_SAMPLE = this.extractor.metadata['A2'].v.split(SAMPLE_DELIMITER).length;
    for (let cell in this.extractor.metadata) {
      if (cell != 'A1' && cell.match(/^A\d+$/)) {
        let current_sample_length = this.extractor.metadata[cell].v.split(SAMPLE_DELIMITER).length;
        if (current_sample_length != LENGTH_OF_1ST_SAMPLE) {
          return [false, cell];
        }
      }
    }
    return [true, ''];
  }

  sampleHaveFiles() {
    const SAMPLE_DELIMITER = /,\s|,|\s/;
    let sampleFilesName = [];
    let filesNameToBeUploaded = this.getFilesName();
    let valid = true;
    let errors = [];
    for (let cell in this.extractor.metadata) {
      if (cell != 'A1' && cell.match(/^A\d+$/)) {
        sampleFilesName.push(...this.extractor.metadata[cell].v.split(SAMPLE_DELIMITER));
      }
    }

    for (let i = 0; i < sampleFilesName.length; i++) {
      let sampleFileName = sampleFilesName[i];
      if (!filesNameToBeUploaded.includes(sampleFileName)) {
        valid = false;
        errors.push(`Uplaoder is missing file ${sampleFileName}`);
      }
    }

    return [valid, errors];
  }

  sheetHasMetadata() {
    return this.extractor.metadata !== undefined;
  }

  sheetHasColumn(columnName) {
    return (
      Object.prototype.hasOwnProperty.call(this.extractor.metadata, 'A1') &&
      this.extractor.metadata['A1'].v == columnName
    );
  }

  sheetHasData() {
    for (let key in this.extractor.metadata) {
      if (key != 'A1' && key.match(/^A\d+$/)) {
        return true;
      }
    }
    return false;
  }

  async getMetadata() {
    let xlsxFile = this.getXLSXFile();
    let extractor = new XLSXMetaExtractor(xlsxFile);
    return await extractor.extract();
  }

  validateFilesName() {
    let errors = [];
    let valid = true;
    for (var i = 0; i < this.files.length; i++) {
      let filename = this.files[i].name;
      if (!filename.endsWith('xlsx')) {
        if (this.filenameHasSpaces(filename)) {
          valid = false;
          errors.push(`Spaces in ${filename} file are not allowed.`);
        }

        let [containValidChar, char] = this.filenameHasValidChars(filename);
        if (!containValidChar) {
          valid = false;
          errors.push(`Character ${char} in ${filename} file is not allowed.`);
        }
      }
    }
    return [valid, errors];
  }

  filenameHasSpaces(filename) {
    let spaceCount = filename.split(' ').length - 1;
    return spaceCount > 0;
  }

  filenameHasValidChars(filename) {
    const ALLOWED_CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_';
    for (var i = 0; i < filename.length; i++) {
      let char = filename[i];
      if (!ALLOWED_CHARS.includes(char)) {
        return [false, char];
      }
    }
    return [true, ''];
  }

  hasXLSXFile() {
    let xlsxFiles = this.files.filter(function(file) {
      return file.name.endsWith('xlsx');
    });
    return xlsxFiles.length > 0;
  }

  hasOnlyOneXLSXFile() {
    let xlsxFiles = this.files.filter(function(file) {
      return file.name.endsWith('xlsx');
    });
    return xlsxFiles.length == 1;
  }

  getXLSXFile() {
    return this.files.filter(function(file) {
      return file.name.endsWith('xlsx');
    })[0];
  }

  validateLevenstein(names) {
    let valid = true;
    let errors = [];
    for (let i = 0; i < names.length; i++) {
      let currentName = names[i];
      let nextName = names[i + 1];
      if (currentName != undefined && nextName != undefined) {
        if (Levenshtein.get(currentName, nextName) > 1) {
          valid = false;
          errors.push(`Looks like ${currentName} and ${nextName} files name are to far apart.`);
        }
      }
    }
    return [valid, errors];
  }

  getFileCount() {
    return this.files.length;
  }

  getXLSXFilesCount() {
    return this.files.length;
  }

  getFilesName() {
    return this.files.map(function(file) {
      return file.name;
    });
  }

  updateValidator(valid, error) {
    if (error && !valid) {
      this.valid = valid;
      if (Array.isArray(error)) {
        this.errors = [...error];
      } else {
        this.errors.push(error);
      }
    }
  }
}
