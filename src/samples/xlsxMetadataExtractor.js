var XLSX = require('xlsx');
import FileReader from './fileReader.js';

export default class XLSXMetadataExtractor {
  xlsxFile = [];
  workbook = undefined;
  metadata = undefined;

  constructor(file) {
    this.xlsxFile = file;
  }

  async extract() {
    let results = await FileReader.readAsBinaryStringPromise(this.xlsxFile);
    this.workbook = XLSX.read(results.data, { type: 'binary' });
    this.metadata = this.workbook.Sheets['metadata'];
  }
}
