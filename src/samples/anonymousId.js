import { v4 as uuidv4 } from 'uuid';
import Storage from './storage.js';

export default function setAnoumousId() {
  let anonymousId = Storage.getCookie('anonymous_id');
  if (!anonymousId) {
    var now = new Date();
    const TEN_YEARS_MS = 1000 * 3600 * 24 * 365 * 10;
    now.setTime(now.getTime() + TEN_YEARS_MS);

    Storage.setCookie('anonymous_id', uuidv4(), now.toGMTString());
  }
}
