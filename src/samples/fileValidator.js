// AUTHOR: ludd@food.dtu.dk
// DESCRIPTION: Responsibility of this class is to validate file content.
import FileReader from './fileReader.js';
import ArrayConverter from './arrayConverter.js';

var PAKO = require('pako');

export default class FileValidator {
  files = [];
  errors = [];
  valid = true;
  FASTQ_WINDOW_SIZE = 4;
  VALIDATORS = {
    fa: 'FASTA',
    fna: 'FASTA',
    fsa: 'FASTA',
    fasta: 'FASTA',
    fq: 'FASTQ',
    fastq: 'FASTQ',
    txt: 'FASTQ',
    gz: 'GZ',
    gbk: 'GBK',
    vcf: 'VCF',
    xlsx: 'XLSX',
  };

  constructor(files) {
    this.files = files;
  }

  async validate() {
    if (this.files.length == 0) {
      this.updateValidator(false, 'This file is not allowed for upload.');
    }

    for (var i = 0; i < this.files.length; i++) {
      let valid, error;
      let file = this.files[i];
      let fileExtention = this.getFileExtention(file.name);

      if (['fa', 'fna', 'fsa', 'fasta', 'fq', 'fastq', 'gbk', 'txt', 'vcf'].includes(fileExtention)) {
        let result = await FileReader.readAsTextPromise(file, 0, 512000);
        switch (this.VALIDATORS[fileExtention]) {
          case 'GBK':
            [valid, error] = this.validateGBK(result);
            break;
          case 'VCF':
            [valid, error] = this.validateVCF(result);
            break;
          case 'FASTQ':
            [valid, error] = this.validateFASTQ(result);
            break;
          default:
            [valid, error] = this.validateFASTA(result);
        }
      }

      if (fileExtention == 'xlsx') {
        let result = await FileReader.readArrayBufferPromise(file, 0, 4);
        [valid, error] = this.validateXLSX(result);
      }

      if (fileExtention == 'gz') {
        let result = await FileReader.readArrayBufferPromise(file, 0, 512000);
        [valid, error] = this.validateGZFile(result);
      }
      this.updateValidator(valid, error);
    }
  }

  validateXLSX(result) {
    let hex = ArrayConverter.arrayBufferToHex(result.data);
    if (!this.isXLSX(hex)) {
      return [
        false,
        `Invalid signature detected! ${result.event.target.file.name} is not an XLSX file.`
      ];
    }
    return [true, ''];
  }

  validateGZFile(result) {
    let hex = ArrayConverter.arrayBufferToHex(result.data.slice(0, 2));
    if (!this.isGZ(hex)) {
      return [
        false,
        `Invalid signature detected! ${result.event.target.file.name} is not an Gzip file.`
      ];
    }

    // Override ArrayBuffer with unziped content
    result.data = PAKO.inflate(result.event.target.result, { to: 'string' });

    let fileExtention = result.event.target.file.name.split('.').slice(-2, -1)[0];
    switch (this.VALIDATORS[fileExtention]) {
      case 'GBK':
        return this.validateGBK(result);
      case 'VCF':
        return this.validateVCF(result);
      case 'FASTQ':
        return this.validateFASTQ(result);
      case 'FASTA':
        return this.validateFASTA(result);
      default:
        return [true, ''];
    }
  }

  validateFASTA(result) {
    var content = result.data;
    var header = content.slice(0, content.indexOf('\n'));

    if (!this.hasContent(content)) {
      return [false, `File ${result.event.target.file.name} is missing content.`];
    }

    if (!this.isValidFASTAHeader(header)) {
      return [false, `Invalid or missing header in file ${result.event.target.file.name}`];
    }

    const SECOND_LINE = content.indexOf('\n') + 1;
    const LAST_LINE = content.length;
    var sequence = content.slice(SECOND_LINE, LAST_LINE);
    if (!this.hasSequence(sequence)) {
      return [false, `File ${result.event.target.file.name} is missing sequence.`];
    }

    var lines = this.contentToLines(content);
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];
      if (this.isFASTAHeader(line) && !this.isValidFASTAHeader(line)) {
        return [false, `Invalid header in file ${result.event.target.file.name} ${line}`];
      }

      if (!this.isFASTAHeader(line) && !this.isValidFASTASequence(line)) {
        return [
          false,
          `Sequence in file ${result.event.target.file.name} seems to be invalid. ${line}`
        ];
      }
    }
    return [true, ''];
  }

  validateFASTQ(result) {
    var content = result.data;

    if (!this.hasContent(content)) {
      return [false, `File ${result.event.target.file.name} is missing content.`];
    }

    if (!this.isValidFASTQHeader(content)) {
      return [false, `Invalid or missing header in file ${result.event.target.file.name}`];
    }

    if (this.isFASTQMinionSequence(content)) {
      return [
        false,
        `Samples sequenced by MinION are not allowed. You cannot upload file ${result.event.target.file.name}`
      ];
    }

    if (!this.hasFASTQMinimumLength(content)) {
      return [
        false,
        `Number of lines per sequence in file ${result.event.target.file.name} does not match with FASTQ specification.`
      ];
    }

    var lines = this.contentToLines(result.data);
    var normSequences = ArrayConverter.arrayToN(lines, this.FASTQ_WINDOW_SIZE);
    for (var index in normSequences) {
      var sequence = normSequences[index];
      var header = sequence[0];
      var dnaSequence = sequence[1];
      var separator = sequence[2];
      var qualityScore = sequence[3];

      if (!this.isValidFASTQHeader(header)) {
        return [
          false,
          `One of the sequence in ${result.event.target.file.name} is missing identifier. ${header}`
        ];
      }

      if (!this.isValidFASTQSequence(dnaSequence)) {
        return [
          false,
          `One of the sequence in file ${result.event.target.file.name} seems to be invalid. ${dnaSequence}`
        ];
      }

      if (!this.isValidFASTQSeparator(separator)) {
        return [
          false,
          `One of the sequence in ${result.event.target.file.name} has invalid separator. ${separator}`
        ];
      }

      if (!this.isValidFASTQQualityScore(qualityScore)) {
        return [
          false,
          `One of the sequence in ${result.event.target.file.name} is missing quality score. ${qualityScore}`
        ];
      }
    }
    return [true, ''];
  }

  validateVCF(result) {
    var content = result.data;

    if (!this.hasContent(content)) {
      return [false, `File ${result.event.target.file.name} is missing content.`];
    }

    if (!this.isValidVCFHeader(content)) {
      return [false, `Invalid or missing header in file ${result.event.target.file.name}`];
    }
    return [true, ''];
  }

  validateGBK(result) {
    var content = result.data;
    if (!this.hasContent(content)) {
      return [false, `File ${result.event.target.file.name} is missing content.`];
    }

    if (!this.hasGENBANKOriginHeader(content)) {
      return [false, `File ${result.event.target.file.name} is missing ORIGIN header.`];
    }

    if (!this.isValidGENBANKSequence(content)) {
      return [false, `File ${result.event.target.file.name} is missing DNA sequence.`];
    }
    return [true, ''];
  }

  hasGENBANKOriginHeader(content) {
    var match = content.match(/ORIGIN\s.+\n/g);
    return match != null && match.length >= 1;
  }

  isValidGENBANKSequence(sequence) {
    var match = sequence.match(/([ABCDEFGHIKLMNPQRSTUVWY]{10}\s){6}/gi);
    return match != null && match.length >= 1;
  }

  isValidFASTQHeader(header) {
    return header != '' && header.startsWith('@');
  }

  isValidFASTQSequence(sequence) {
    var match = sequence.match(/[ABCDEFGHIKLMNPQRSTUVWY]+/gi);
    return match != null && match.length == 1;
  }

  isValidFASTQSeparator(separator) {
    return separator.startsWith('+');
  }

  isValidFASTQQualityScore(socre) {
    return socre != '' && socre.length > 1;
  }

  contentToLines(content) {
    return content.split(/\n/g).filter(function(line) {
      return line != '';
    });
  }

  hasFASTQMinimumLength(content) {
    return content.split(/\n/g).slice(0, -1).length >= this.FASTQ_WINDOW_SIZE;
  }

  isFASTQMinionSequence(content) {
    return content.match(/fast5|minion/gim) != null;
  }

  hasSequence(sequence) {
    return sequence.length > 0;
  }

  hasContent(content) {
    return content != '';
  }

  isFASTAHeader(line) {
    return line.includes('>');
  }

  isValidFASTAHeader(header) {
    return header != '' && header.startsWith('>');
  }

  isValidVCFHeader(header) {
    var match = header.match(/^#CHROM.+\n/gm);
    return match != null && match.length == 1;
  }

  // NOTE: https://www.bioinformatics.org/sms/iupac.html
  isValidFASTASequence(sequence) {
    var match = sequence.match(/[ABCDEFGHIKLMNPQRSTUVWY\-\.\*]+/gi);
    return match != null && match.length == 1;
  }

  getFileExtention(filename) {
    return filename.split('.').pop();
  }

  updateValidator(valid, error) {
    if (error && !valid) {
      this.valid = valid;
      this.errors.push(error);
    }
  }

  isGZ(signature) {
    return '1f8b' == signature;
  }

  isXLSX(signature) {
    return '504b34' == signature;
  }
}
