export default class SampleValidator {
  valid = true;
  errors = [];
  samples = [];

  constructor(samples) {
    this.samples = samples;
  }

  validate() {
    let valid, error;

    [valid, error] = this.hasDuplicatedFingerprints();

    this.updateValidator(valid, error);
  }

  hasDuplicatedFingerprints() {
    let allFingerprints = {};

    // create data structure for duplicated fingerprints
    for (let sample of this.samples) {
      for (let fingerprint of sample.fingerprint) {
        if (!Object.hasOwnProperty.call(allFingerprints, fingerprint)) {
          allFingerprints[fingerprint] = [sample.cell];
        } else {
          allFingerprints[fingerprint].push(sample.cell);
        }
      }
    }

    // find duplicated fingerprints
    let error = [];
    let valid = true;
    for (let fingerprint in allFingerprints) {
      let errMsg = 'Files';
      if (allFingerprints[fingerprint].length > 1) {
        let files = this.findFileByFingerprint(fingerprint);
        for (let file of files) {
          if (!file.cell) {
            errMsg += ` ${file.name}, `;
          } else {
            errMsg += ` ${file.name}, `;
            // errMsg += ` ${file.name} cell ${file.cell},`
          }
        }
        errMsg += ' have the same content.';
        error.push(errMsg);
        valid = false;
      }
    }

    return [valid, error];
  }

  findFileByFingerprint(fingerprint) {
    let files = [];
    for (let sample of this.samples) {
      for (var i = 0; i < sample.fingerprint.length; i++) {
        if (sample.fingerprint[i] == fingerprint) {
          files.push(sample.files[i]);
        }
      }
    }
    return files;
  }

  updateValidator(valid, error) {
    if (error && !valid) {
      this.valid = valid;
      if (Array.isArray(error)) {
        this.errors = [...error];
      } else {
        this.errors.push(error);
      }
    }
  }
}
