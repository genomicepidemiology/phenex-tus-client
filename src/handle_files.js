import FileValidator from './samples/fileValidator.js';
import XLSXValidator from './samples/xlsxValidator.js';
import SampleExtractor from './samples/sampleExtractor.js';
import SampleValidator from './samples/sampleValidator.js';

export default async function handleFiles(files, sampleMetadata, callbacks) {
  let valid, errors;
  [valid, errors] = await validateFiles(files);
  if (!valid) {
    callbacks.onError(errors);
    return false;
  }

  [valid, errors] = await validateXLSX(files);
  if (!valid) {
    callbacks.onError(errors);
    return false;
  }

  let samples = await extractSamples(files);
  normalizeSamples(samples, sampleMetadata);
  await fingerprintSamples(samples);

  [valid, errors] = validateSamples(samples);
  if (!valid) {
    callbacks.onError(errors);
    return false;
  }

  callbacks.onValidationSuccess(samples);
  return samples;
}

async function validateFiles(files) {
  let validator = new FileValidator(files);
  await validator.validate();
  return [validator.valid, validator.errors];
}

async function validateXLSX(files) {
  let validator = new XLSXValidator(files);
  await validator.validate();
  return [validator.valid, validator.errors];
}

async function extractSamples(files) {
  let sex = new SampleExtractor(files);
  await sex.extract();
  return sex.samples;
}

function normalizeSamples(samples, metadata) {
  for (let sample of samples) {
    sample.indexFiles();
    sample.initializeFileUploadProgress();
    sample.addMetadata(metadata);
  }
}

async function fingerprintSamples(samples) {
  for (let sample of samples) {
    const MiB = 1048576;
    await sample.generateFingerprint(5 * MiB);
  }
}

function validateSamples(samples) {
  let validator = new SampleValidator(samples);
  validator.validate();
  return [validator.valid, validator.errors];
}
