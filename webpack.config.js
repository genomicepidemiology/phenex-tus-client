const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    bundle: './src/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'dtu_tusfull_client.min.js',
    library: 'dtu_tus_client',
    libraryTarget: 'var'
        
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/
    }]
  },
  node: {
    process: false,
    Buffer: false
  }
};
